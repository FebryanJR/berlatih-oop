<?php 
require_once ('Animal.php');
require_once ('Frog.php');
require_once ('Ape.php');

$sheep = new Animal("shaun");
echo "Namanya : ". $sheep->name; // "shaun"
echo "<br>";
echo "Kakinya : ". $sheep->legs; // 2
echo "<br>";
echo "Berdarah Dingin : ". $sheep->cold_blooded; // false
echo "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "Namanya : ". $sungokong->name; // "kera sakti"
echo "<br>";
echo "Kakinya : ". $sungokong->legs; // 2
echo "<br>";
echo "Berdarah Dingin : ". $sungokong->cold_blooded; // False
echo "<br>";
echo "Bunyinya : ". $sungokong->yell(); // 'Auooo'
echo "<br>";
echo "<br>";

$kodok = new Frog ("buduk", 4, "True");
echo "Namanya : ". $kodok->name; // "buduk"
echo "<br>";
echo "Kakinya : ". $kodok->legs; // 4
echo "<br>";
echo "Berdarah Dingin : ". $kodok->cold_blooded; // True
echo "<br>";
echo "Lompat : ". $kodok->jump() ; // "hop hop"

 ?>